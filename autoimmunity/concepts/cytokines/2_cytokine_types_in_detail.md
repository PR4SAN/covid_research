# Cytokine and chemokine Panels

Below cytokines are taken from the [cytokine panel](../../../diagnotic_tests/blood_tests.md#Inflammation) and the [Incelldx covid long hauler panel](https://covidlonghaulers.com/).

![Incelldx Covid long haulers cytokine panel](../images/IncellDx_LH_cytokine_panel.png)

## Tumor necrosis factor (TNF)

### TNF Alpha

- multifunctional cytokine secreted primarily by activated monocytes/macrophages.
- potent paracrine and endocrine mediator of inflammatory and immune functions.
- regulates growth and differentiation of a wide variety of cells types.

## Haematopoietins

![Colony Stimulating Factors](../images/colony_stimulating_factor.png)

### M-CSF and GM-CSF

- M-CSF - Macrophage Colony Stimulating Factor
- GM-CSF -Granulocyte/Macrophage Colony Stimulating Factor

M-CSF and GM-CSF role is to trigger the proliferation of bone marrow precursors and differentiate them to macrophages, granulocyte/macrophage colonies respectively.

## Soluble CD40 Ligand (SCD40L)

- contained in platelet granules and its **presence in the blood is a marker of platelet activation**.
- Interacts with `CD40` on endothelial cells and may trigger inflammation and coagulation cascade.

## Vascular Endothelial Growth Factor (VEGF) aka Vascular Permeability Factor

- signalling protein produced by cells
- This signalling stimulates formation of blood vessels(vasculogenesis and angiogenesis).
- helps in creating new blood vessels after injury

## Interleukins

### Interleukin-1

- Source - macrophages, B cells, monocytes, dendritic cells
- family of proinflammatory cytokines that regualate and initiate inflammatory responses.
- expression of integrins

### Interleukin-2

- activated `CD4+` and `CD8+` T-cells release IL-2
- response to microbial infection and discriminating between self and antigen

### Interleukin-4

- Source - activated naive `CD4+` cell, memory `CD4+`, mast cells and macrophages
- produced by `CD4+` T-cells to signal B-cells to proliferate and undergo class switching.

### Interleukin-6

- macrophages, Th2 cells, B cells, astrocytes, endothelium
- Blood vessels produced IL-6 as pro-inflammatory
- plays a vital role in the final differentiation of B-cells into plasma B-cells.

### Interleukin-8

- its a chemokine produced by macrophages, lymphocytes, epithelial and endothelial cells.
- Cells have GPCR receptors (CXCR1 and CXCR2) for IL-8
- important mediator of the innate immune response.

### Interleukin-10

- monocytes, Th2 cells, CD8+ T cells, mast cells, macrophages, B cell subset
- Inhibits synthesis of cytokines, IFN-gamma, IL-2, IL-3, TNF and GM-CSF produced by activated macrophages and T-cells.

### Interleukin-13

- activated Th2 cells, mast cells, NK cells
- inhibits inflammatory cytokine production and synergises with IL-2 in regulating interferon-gamma synthesis.

## Interferons

### Interferon alpha

- produced by dendritic cells.
- involved in innate immunity against viral infection.

### Interferon Beta

- produced in large quantities by fibroblasts.
- antiviral activity involving innate immune response.

### Interferon gamma

- activated by IL-12
- released by killer T-cells ,type1 helper T-cells(adaptive immune response)
- also released by NK cells and NK T-cells as part of innate immune response
- inducer of MHC-II expression which is recognized by helper T-cells
- directly activate macrophages and natural killer cells.

Aberrant `IFN-gamma` is associated with autoimmune and autoinflammatory diseases.

## Chemokines

Chemokines are responsible for directional movement of leukocyte. Some chemokines are pro-inflammatory and they play important role in the recruitment of immune cells at the site of infection. Below ones are inflammatory.

Migration of monocytes from the blood stream across the vascular endothelium is required for routine immunological surveillance of tissues, as well as in response to inflammation. Chemokines facilitate this migration.

These chemokines affect monocytes/macrophages, T cells, dendritic cells, NK cells and platelets, granulocytes(particularly neutrophils)

CCL3 and CCL4 produced particularly by macrophages, dendritic cells and lymphocytes. These MIP-1 chemokines are also responsible for release of IL-1, IL-6 and TNF-alpha from fibroblasts(wound healing) and macrophages.

### CCL2 (Monocyte Chemoattractant Protein-1)

- recruits monocytes/macrophages, memory T-cells, dendritic cells at the site of inflammation from tissue damage or infection.
- binds to CCR2 and CCR4 cell surface receptors(GPCR)

### CCL3 (Macrophage Inflammatory Protein-1 ALPHA)

- Macrophage inflammatory protein alpha(MIP)
- Proinflammatory cytokines like **IL-1Beta** can stimulate its production
- Attracts monocytes, macrophages and neutrophils.
- recruitment and activation by binding to receptors(GPCR) CCR1, CCR4 and CCR5
- induces fever.
- This chemokine interacts with CCL4.

**NOTE**- - Granulocytes are of neutrophils(most abundant), eosinophils, basophils and mast cells.

### CCL4 (Macrophage Inflammatory Protein-1 BETA)

- Macrophage inflammatory protein beta(MIP)
- chemoattractant for monocytes, NK cells, other immune cells
- produced by neutrophils, monocytes, B-cells, T-cells, fibroblasts, endothelial cells, and epithelial cells.
- binds to receptors(GPCR) CCR5 and CCR8.

### CCL5 (RANTES)

- RANTES - Regulated on Activation, Normal T-cell Expressed and Secreted
- expressed by T-cells, monocytes, epithelial cells, fibroblasts and platelets.
- highest binding affinity to CCR5 receptor.
- chemotactic(induce movement) for T cells, granulocytes, macrophages, natural-killer (NK) cells, dendritic cells and mast cells.
- Along with `IL-2` and `IFN-gamma` from T-cells, CCL5 induces proliferation and activation of NK cells
- CCL5 released by virus-specific activated killer(CD8+) T-cells
