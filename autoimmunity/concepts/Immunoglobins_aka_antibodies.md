# Immunoglobins

## Introduction

- Immunoglobin aka antibody produced by B-cells

![Immunoglobin](images/immunoglobin.png)

## Biological functions

- Neutralize antigen
- Activation of the complement system via the antigen-antibody immune complex
- Opsonization - make antigen recognizable by receptors on the surface of the phagocytes for phagocytosis.

## Immunoglobin genomic organization

![Antibody structure](images/antibody_structure.png)

- 2 light and 2 heavy chains
- Variable region that acts as contact site for the antigen
- Constant region that enable protein assembly and effector functions.
- Both light and heavy chain have these two regions.
- Light chain divided in to 2 domains/regions (VL and CL)
- Heavy chain has four domains (CH1, CH2, CH3 and VH)
- Antigen binding region is called **Fab** region (arms of `Y`)
- **Fc**(Fragment crystallizable) region has Fc receptors that either binds to phagocytes, cytotoxic T-cells or perform complement activation.
- Constant region on the heavy chain decides the isotype and subclass of the immunoglobin.

### Isotype

![Antibody Isotypes](images/antibody_isotypes.jpg)

- Sequences of heavy chain constant regions are of 5 types named with Greek letters that correspond to the isotype as follows
  `μ(mu)` - IgM
  `𝛿 (delta)` - IgD
  `γ (gamma)` - IgG
  `ε (epsilon)` - IgE and
  `α (alpha)` - IgA

(Remember it as D for Delta, M for Mu, E for Epsilon)

- Isotype IgM is the first to be produced. As the infection progresses, isotype switching happens.

## Subclasses

IgA and IgG can be divided into several subclasses.

## Immunoglobin A (IgA) subclasses

- This is the first line of defense against infection of epithelial cells.
- IgA1 found in serum while IgA2 is the secretary form of IgA
- IgA2 is responsible for IgA-mediated excretory pathway to eliminate antigen.
- IgA2 has role in adaptive immune defense at the mucosal surfaces(respiratory, gastrointestinal, urogenital). Due to this, IgA is predominantly found in human secreations, tears.
- IL-10, IL-4 and Transforming Growth Gactor beta (TGF-beta) involved in B-cell class switching and differentiation.

## IgG Subclasses

Here we will discuss about the IgG subclasses.

- Four distinct subclasses IgG1, IgG2, IgG3 and IgG4.
- IgG subclass Switching is regulated by interaction with T-cells.
- IgG subclass switching is unidirectional (IgG3 -> IgG1 -> IgG2 -> IgG4)
- Based on their structure, these antibodies can either initiate complement activation, recruitment of various cells by Fc receptors and agonistic signalling.

Three arbitrary patterns

- Only complement binding subclasses (IgG3 and/or IgG1)
- expansion to noncomplement-binding subclasses (IgG3 and/or IgG1 + IgG2 and/or IgG4)
- switch to noncomplement-binding subclasses (IgG2 and/or IgG4)

Third group prevalence is `< 5%`, while the first two groups have close to `50%`

## Basics of class switching

- As we known the B-cells produce the antibodies. So B-cells will undergo isotype and subclass switching.
- Initially B-cells produce IgM antibodies.
- Only antigen-experienced B-cells undergo class switching to other isotypes.

Nature of antigen influences IgG subclass production.

- T-independent polysaccharide or glycolipid antigens induce IgM and IgG2
- T-independent protein antigens stimulate IgG1 and IgG3
- IgG4 switching happens in allergy(chronic antigen stimulation)
- T-dependent protein antigen stimulate follicular B-cells that require `CD4+`(Helper) T-cell signalling to clonal selection of B-cell to plasma B-cell that can produce class switched Igs and memory B-cells.
- Activated T-cells express `CD40L` aka `CD154` which interact with `CD40` on B-cells driving B-cell activation and maturation. This interaction is very important in producing high affinity IgG antibodies.

Cytokines signal B-cell the following

- Isotype switch
- Subclass of Ig switch
- Secrete the switched isotype and subclass of Ig

Cytokines IL-4, IL-13, and IL-10 influence the isotype and subclass production, but we don't know exactly which one does what.

**Isotype and Subclass switching is a sequential process and once B-cell has switched to downstream isotype or subclass, it cannot switch upstream.**

## STRUCTURE AND EFFECTOR FUNCTIONS OF IGG SUBCLASSES

- IgG1 most abundant subclass
- IgG3 has the shortest half-life of only 7 days. IgG3 has the longest Hinge region yielding most flexibility and accessibility.
- IgG2 and IgG4 hinger regions are much shorter and hence more rigidity.

### Complement Activation

- IgG3 and IgG1 exhibit most efficient complement activation, IgG2 in certain conditions while IgG4 is incapable.
- Complement C1-complex binds to the Fc region of IgG.
- Only very high titers of antibody and/or stronger C1q binding subclasses can trigger terminal complement activation leading to cell lysis(disruption)

### Fc receptors

- Effector functions are phagocytosis, degranulation and Antibody dependent cell-mediated cytotoxicity(ADCC)
- These effector functions are carried out by adaptive and innate immune cells.
- Receptors for Fc region are expressed by B-cells, monocytes, natural killer cells, neutrophils, mast cells and occasionally T cells.
- The human `FcγR` system in IgG(note the Gamma in the Fc region name) is composed of 3 major classes, `FcγRI` (CD64), `FcγRII` (CD32) and `FcγRIII` (CD16).
- Most `FcγR` are activating in nature.

- IgG3 and IgG1 bind well to monocyte `FcγRI`
- Neutrophil `FcγRIIA`, and `FcγRIIIB`
- NK cell `FcγRIIIA`.
- IgG2 can only be effectively recognized by 1 allele of `FcγRIIA`, whereas IgG4 is only bound by `FcγRI`

### Agonistic signalling

- Antibody itself could bind to and cause activation of a receptor, thereby initiating the downstream process - Agonistic signalling
- In agonistic signalling, endogenous ligand or neurotransmitter is mimicked.
- It is hypothesized that such agonistic signaling promotes endothelial dysfunction.
- In theory, all IgG subclasses should be capable of triggering activation of endothelial cells.

## Sidenote

### CD40L - CD40 interaction in T-cells and B-cells

- When T-cell calcium influx if up-regulated, T-cells remain activated and sustain the `CD154` aka `CD40L` expression.
- This can be found in the form of elevated `CD40L` levels in the serum (`SCD40L`). This can sometimes lead to **Hypergammaglobulinemia** which is nothing but elevated levels of immunoglobins in the blood. Elevated levels of CD154 in serum is observed in patients with [Sjogren's Syndrome][1].

### CD154 Receptor in Platelets

- CD40L(Ligand) aka CD154 is reported to be present in platelets after activation.
- This CD154 on the platelet surface can interact with CD40 on the endothelial cells and cause inflammatory response.
- The `CD154/CD40` system is considered to have an important role in maintaining inflammatory responses and to be involved in atherosclerosis.

---

## References

- [Antibody: Basic structure and Biological function](https://onlinesciencenotes.com/antibody-basic-structure-and-biological-function/)
- [The Biology of IgG Subclasses and Their Clinical Relevance to Transplantation](https://journals.lww.com/transplantjournal/Fulltext/2018/01001/The_Biology_of_IgG_Subclasses_and_Their_Clinical.2.aspx)
- [Chapter 9 - Platelet Receptors](https://www.sciencedirect.com/science/article/pii/B9780123878373000092)
- [Immunoglobulin A (IgA)](https://www.immunology.org/public-information/bitesized-immunology/receptors-and-molecules/immunoglobulin-iga)

[1]: https://www.sciencedirect.com/science/article/pii/B9780128036044000198
