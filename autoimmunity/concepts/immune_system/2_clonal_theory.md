# Activation of the lymphocytes

## The Clonal Selection Theory

- Each B-cell makes one kind of **antibody receptor**. We have billions of B-cells
- Presence of antigen causes the B-cell that has the apt antibody receptor to multiply and differentiate.
- Helper T-cell should send a stimulatory signal to allow selection of that particular B-cell. Thus helper T-cells regulate the process.
- This clonal selection then produces memory B cells as well as plasma B-cells. Plasma B-cells produce the antibody that binds to the antigen with high affinity.

![T cell and](./t_cell_memory_plasma_cell.gif)

## Activation of T cells

## Helper T cells

Two signals are required to activate helper T cells.

- Antigen presenting cell ingests the microbe and exports fragments to cell surface along with MHC molecules to which helper T cells have receptors.
- Second signal through stimulation by a cytokine or costimulatory response between surface protein of the affected cell and helper T cell CD28 protein.

> If only the first signal is received, the T cell may be rendered anergic—that is, unable to respond to antigen.

> A discussion of helper-T-cell activation is complicated by the fact that helper T cells are not a uniform group of cells but rather can be divided into two general subpopulations—TH1 and TH2 cells—that have significantly different chemistry and function. These populations can be distinguished by the cytokines they secrete. TH1 cells primarily produce the cytokines gamma interferon, tumour necrosis factor-beta, and interleukin-2 (IL-2), while TH2 cells mainly synthesize the interleukins IL-4, IL-5, IL-6, IL-9, IL-10, and IL-13. The main role of the TH1 cells is to stimulate cell-mediated responses (those involving cytotoxic T cells and macrophages), while TH2 cells primarily assist in stimulating B cells to make antibodies.

## Activation of B Cells

A B-cell becomes activated when

- Its receptor recognizes an antigen and binds to it.
- Or In most cases, however, when activated by an activated helper T cell.

Once a helper T cell has been activated by an antigen, it becomes capable of activating a B cell that has already encountered the same antigen. Activation is carried out through a cell-to-cell interaction that occurs between a protein called the CD40 ligand, which appears on the surface of the activated helper T cells, and the CD40 protein on the B-cell surface.

Antigens that induce a response in this manner, which is the typical method of B-cell activation, are called **T-dependent antigens**.

B-cells on activation undergoes the clonal selection (i.e) multiplying and differentiating in to memory B-cells and Plasma B-cells

Each plasma cell can secrete several thousand molecules of immunoglobulin(antibody) every minute into blood circulation and continue to do so for several days.

The initial burst of antibody production gradually decreases as the stimulus is removed (e.g., by recovery from infection), but some antibody continues to be present for several months afterward.

Antibodies protect simply by combining with the repeating protein units that make up these structures of microbes, although they do not kill or dispose of the microbes. The actual destruction of microbes involves **phagocytosis** by **granulocytes** and **macrophages**, and this is greatly facilitated by the participation of the complement system.

## References

- [The Clonal Selection Theory](http://www.biology.arizona.edu/immunology/tutorials/immunology/page4.html)
- [Activation of T and B lymphocytes](https://www.britannica.com/science/immune-system/Activation-of-T-and-B-lymphocytes)
