# Introduction

Immune system comprises of complex network of cells and protein with the purpose of protecting body against infection.

Immune system consists of the following

- White blood cells
- Antibodies
- The Complement system
- The Lymphatic system
- Spleen
- Thymus
- Bone marrow

Immune system keeps track of all the microbes it defeated in the past. This information is stored in what is called as memory cells which are types of white blood cells i.e B and T cells.

In case of flu or common cold, mutation(new strain of the same virus) escapes this memory and hence the immune system will have to fight those like a new virus.

![Immunce system cells](./immune_system.gif)

## Immune system types

The immune system is typically divided into two categories--innate(nonspecific defense mechanism) and adaptive(antigen specific)--although these distinctions are not mutually exclusive.

> The antigen first must be processed and recognized. Once an antigen has been recognized, the adaptive immune system creates an army of immune cells specifically designed to attack that antigen. Adaptive immunity also includes a "memory" that makes future responses against a specific antigen more efficient.

> The memory of antigen and the stimulated response is the basis for success in vaccination programs.

## Bone marrow

Bone marrow is the spongy tissue found inside your bones. It produces the red blood cells our bodies need to carry oxygen, the white blood cells we use to fight infection, and the platelets we need to help our blood clot.

## Spleen

Spleen acts like a blood filtering system. Removes/destroys damaged as well as dead RBCs from blood. It also stores platelets and WBCs.

## WBC

- WBCs are made in bone marrow.
- Part of the lymphatic system
- Looks for foreing invaders(virus, bacteria, parasites, fungi) throughout the body
- Include lymphocytes such as B-cells, T-cells, natural killer cells etc

### [Lymphocytes](https://www.medicalnewstoday.com/articles/320987)

- type of WBC and main type of immune cells
- Found in blood and lymph tissue
- these primarily should attack microbes but sometimes attack our own body cells.
- normal range is between 1000-4800 lymphocytes in $$1\mu L$$
- viral infections among other causes can lead to lymphocytes to either go up (lymphocytosis) or go down (lymphocytopenia).

#### Types

- B cells and T cells are two main categories of lymphocytes.
- Lymphocytes originate from stem cells in the bone marrow.
- Those lymphocytes that reach **Thymus** become T-cells and those that stay in the **Bone marrow become B-cells**.
- B-cells create antibodies to antigen(foreign substances).
- **Each B-cell creates one specific antibody. Each antibody matches an antigen for destruction. The process of matching is similar to how a key fits into a lock.**
- T-cells regulate immune responses by killing cancerous cells or those that have been invaded by virus(ex: Covid gains entry into cells via ACE2 receptor).

> A third type of lymphocyte, **known as a natural killer or NK cell, comes from the same place as B and T cells**. NK cells respond quickly to several foreign substances and are specialized in killing cancer cells and virus-infected cells.

- I have read that immune system can be of innate as well as adaptive. Natural killer cells could make up the innate immune response while B-cells and T-cells could make up the adaptive immune response.

#### B Cells

- categorized in to Memory B-cells, regulatory B-cells(Bregs) and Plasma cells

- Memory B-cells remain in body for decades, remembering its antigens and help immune system respond faster to future attacks from the same antigen that it recognizes. Remember B-cell to antigen is one to one, each memory B-cell will know how to kill a single antigen.

- Bregs though fewer in number in healthy people they have protective anti-inflammatory effects like stopping lymphocytes that cause inflammation. They also promote the production of regulatory T cells(Tregs).

**NOTE**- Autoimmune disorders or problems in the way immune system work can lead to [chronic inflammation](https://www.medicalnewstoday.com/articles/248423#treatment) which inturn can lead to so many issues in the body. NSAIDs dont treat the root cause rather deal with enzymes that contribute to inflammation. Corticosterionds are immunosuppresants.

- **Plasma cells are terminally differentiated B cells that produce antibodies and are responsible for antibody-mediated immunity**.

**NOTE**- Terminally differentiated cells are cells that become specialized to a point after which they can no longer divide.

#### T Cells

- categorized in to Killer, Helper, Regulatory, memory and natural killer T cells

- Killer or cytotoxic T cells scan the cells to see if they are pathogen infected or foreign cells(transplant) or cancer cells and kill those. These T cells contain surface protein called **CD8**. The CD8 co-receptor plays a role in T cell signaling and aiding with **cytotoxic T cell antigen interactions**.
- Helper T cells help start immune response against foreign susbtances. These T cells have a surface protein **CD4**. They help activate cytotoxic T cells and macrophages to attack infected cells, or they stimulate B cells to secrete antibodies. The **CD4** receptor plays a crucial role in the immune system, especially during T cell activation in which it can fulfill an adhesion or signaling function and enhance sensitivity of T cells to antigens.
- Tregs can be helpful or harmful depending on what it regulates(suppress or control). Regulatory T cells may be similar to cytotoxic T cells, but they are detected by their ability to suppress the action of B cells or even of helper T cells (perhaps by killing them). Regulatory T cells thus act to damp down the immune response and can sometimes predominate so as to suppress it completely.
- Memory T cells protect the body against antigens that they have previously identified. **They live for a long time after an infection is over, helping the immune system remember previous infections**. If the same germ enters the body a second time, memory T cells remember it and quickly multiply, helping the body fight it more quickly.
- Natural killer T cells are a mixed group of T cells that share characteristics of **both T cells and natural killer cells**

In short T cells communicate with B Cells to deliver the immune response. Helper T cell might signal B-cells to kill an antigen, while killer T cell might send signal to B-cell to kill infected cell.

## References

- [Immune system explained](https://www.betterhealth.vic.gov.au/health/conditionsandtreatments/immune-system)
- [Activation of T and B cells](https://www.britannica.com/science/immune-system/Activation-of-T-and-B-lymphocytes)
