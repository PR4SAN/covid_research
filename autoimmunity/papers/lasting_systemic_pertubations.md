# SARS-CoV-2 infection results in lasting and systemic perturbations post recovery

---

## References

- [SARS-CoV-2 infection results in lasting and systemic perturbations post recovery](https://www.biorxiv.org/content/10.1101/2022.01.18.476786v1.full)
