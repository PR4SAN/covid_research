# Physiology, Coagulation Pathways

- Function of coagulation is to maintain hemostasis by blocking the bleeding.

## Function

- Primary hemostasis involves forming platelet aggregates to form a plug at the site of endothelial damage.

- Secondary hemostasis involves two pathways extrinsic and intrinsic and both converge at later point to lead to common pathway.

- Common pathway => Fibrinogen to fibrin which combine to form fibrin strands to stabilize the platelet plug

## Clotting factors

- Intrinic pathway(exposed endothelial collagen) factors - I, II, IX, X, XI, XII
- Extrinsic pathway(external damage like wound) factors - I, II, VII and X
- Common pathway factors - I, II, V, VIII and X

Factor Names

- I - Fibrinogen
- II - Prothrombin
- III - Tissue Factor
- VII - Stable factor
- IX - Christmas Factor
- X - Stuart-Prower factor
- XI - Plasma thromboplastin
- XII - Hageman factor

Factors circulate in blood as zymogens which are activated to serine proteases. These serine proteases catalyze activation of more zymogens. Cascade of zymogen activation. These serine proteases ultimately activate fibrinogen.

Zymogens activated into Serine proteases are Factors II, VII, IX, X, XI, XII, basically all factors in intrinsic and extrinsic pathways except for Factor I (fibrinogen).

## Interpreting Terminologies

- Factor XII(zymogen) becomes XIIA(serine protease) after activation. Similar pattern follows for other factors.

## Intrinsic pathway

- Longer pathway out of the two.

- Endothelial damage -> endothelial collagen exposed -> Factor XII -> Factor XIIA

- Factor XIIA -> activates XI -> XIA -> activates IX -> IXA ->activates X -> XA. This is cascade.

- Concentration of factors increases when we move down the above cascade hierarchy. `Conc(X) > Conc(IX)`

- Factor IIA(Thrombin) -> Positive feedback to factors V, VII,VIII, XI and XIII.

- Intrinsic pathway measured as the Partial Thromboplastin Time(factor XI). Because of the positive feedback, it makes sense to start from Factor XI rather than XII.

## Extrinsic pathway

- Shorter pathway

- Blood vessel damage -> Endothelial cells release Factor III -> activates VII -> VIIA -> activates X -> XA

- Clinically measured as Prothrombin Time(PT)

## Common pathway

- Pathway begins here X -> XA. Tenase is responsible for this activation.

- Extrinsic tenase complex consists of -> Factor VII, III and `Ca2+`
- Intrinsic tenase factor made of -> Factor VIII, IXA, a phospholipid and `Ca2+`

- XA along with cofactor Factor V-> Activates II(prothrombin) -> IIA(thrombin) -> Fibrinogen(Factor I) -> Fibrin -> fibrin strands -> Factor XIII -> forms fibrin mesh

- Mesh stabilizes the platelet plug

## Negative feedback

- Prevent overcoagulation and avoid thrombosis.

- IIA(Thrombin) -> activates plasminogen to plasmin -> plasmin breaks down fibrin mesh
- IIA(Thrombin) -> stimulate antithrombin(AT) production -> AT decreases activation of II to IIA and decreases amount of XA.

- Protein C and S act as natural anticoagulants.
- Protein C -> inactivates Factor V and Protein S -> inactivates Factor VIII (common pathway factors which form part of positive feedback loop)

## Organs involved

- Liver responsible for all the factors I, II, V, VII, VIII, IX, X, XI, XIII, and protein C and S.

- Decrease in coagulation factors means liver damage.

## Pathophysiology

- Hemophilia A deficiency in Factor VIII(common pathway). More serious
- Hemophilia B deficiency in Factor IX(intrinsic)
- Hemophilia C deficiency in Factor XI(intrinsic)

Factor V Leiden -> its a genetic mutation which causes hypercoagulability by making factor V unresponsive to Protein C(natural anticoagulant becomes useless in this case)

Deficiencies in protein C and S also can lead to hypercoagulable states

Vitamin K deficiency can lead to elevated PT and PTT. Without vitamin K more coagulation factors cannot be produced by the liver.

---

## References

- [Physiology, Coagulation Pathways](https://www.ncbi.nlm.nih.gov/books/NBK482253/)
