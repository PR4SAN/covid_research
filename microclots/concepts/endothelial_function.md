# Endothelial cells

## Endothelium vs Epithelium

Epithelial cells line the internal and external surfaces of the body.

Endothelial cells are specialized epithelial cells that only line the internal surfaces of the components of the circulatory system(blood vessels and capillaries).

---

## References

- [Stimulated Tissue Plasminogen Activator Release as a Marker of Endothelial Function in Humans](https://www.ahajournals.org/doi/10.1161/01.ATV.0000189309.05924.88)
