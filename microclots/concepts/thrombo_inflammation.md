# Thrombo inflammation

## Terminologies

- Invivo - Within living organism
- Invitro - Outside of living organism(within glass)

## Platelet Adhesion to Vascular Walls

- Platelets always circulate close to the vessel walls.
- In normal circumstances, endothelial cells resist thrombosis.
- Endothelial disruption exposes the subendothelial matrix (intrinsic pathway) which triggers coagulation by platelet activation.

### von Willebrand Factor

vWF is a multimeric protein synthesized by endothelial cells and megakaryocytes. It mediates platelet vessel wall interaction and also serves as carrier molecule for coagulation factor VIII. It is contained in

- Weibel-Palade bodies of endothelial cells,
- `α-granules` of platelets
- soluble form in plasma and
- **the subendothelial matrix**

- Role of vWF in platelet adhesion is determined by **Shear rate**.
- Shear rate - direct function of blood flow velocity and inverse function of cross sectional area.
- Platelet adhesion occur at higher shear rates which means high velocity and microvessels..

### Collagen

- Endothelial cells sit between the platelets in blood and the collagen in the subendothelial matrix.
- First platelet capture has to happen and is mediated by GPIb-vWF interactions.- Then platelet receptors `GPVI` and `α2β1` mediate the interaction with collagen

### Endogenous Mechanisms Preventing Platelet Adhesion to Endothelium

#### Nitric oxide

- endothelial derived relaxing factor.
- NO is derived from L-arginine and oxygen in the presence of several factors.
- Endothelial cells may release NO in response to agonists(histamine, ATP) binding to endothelial receptors or blood flow-induced shear stress.
- `NO` plays a crucial role in inhibiting platelet adhesion and activation.
- `NO` may also inhibit leukocyte adhesion to vascular endothelium

#### Prostacyclin

- main product of arachidonic acid metabolism on endothelial cells
- Endothelial cell derived relaxing factor.
- Vasodilator and platelet aggregation inhibitor.

#### Ecto-adenosine diphosphatase (CD39)

- an integral component of endothelial cell surface membrane
- Antiplatelet effects mediated by metabolizing ADP which is a potent stimulator of platelet aggregation.

#### Endothelial Surface Layer

- Interface between blood and the endothelial cell membrane. This layer is known as glycocalyx.
- Physiologically relevant in the regulation of endothelial permeability, responses to shear stress and leukocyte-endothelial interactions.
- Platelets adhesion upon glycocalyx disruption.

## Platelet adhesion mechanisms

Platelet adhesion can occur when

- Endogenous platelet adhesion mechanisms(above) are inhibited
- Release of molecules by endothelium that promote platelet adhesion

Platelet adhesion happen in inflammatory states. Inflammatory conditions result in leukocyte adhesion to endothelial cells.

### Endothelial P-Selectin Dependent mechanisms

- Endothelium upon activation releases P-selectin. This exposure on the endothelium surface may promote platelet adhesion.
- mediates Initial transient interactions of leukocytes with endothelial cells.
- Leukocyte dependent platelet adhesion in ischemic(reduced blood flow and oxygen)
- There is inter-dependence in leukocyte and platelet adhesion to endothelial cells.

### Endothelial vWF-Dependent Mechanisms

- vWF released in response to similar stimuli that causes to release P-Selection.
- This release vWF(UltraLarge) is larger and more adhesive

> Conceivably, ADAMTS-13 and vWF may become future therapeutic targets in thrombosis associated with inflammation

---

> Adhesive interactions of platelets with endothelial cells are increasingly recognized in inflammatory states, and they provide insight into the known links between inflammation and thrombosis.

## References

- [Chapter 3Platelet Adhesion to Vascular Walls](https://www.ncbi.nlm.nih.gov/books/NBK53456/)
- [Interaction between platelets and endothelium: from pathophysiology to new therapeutic options](https://www.ncbi.nlm.nih.gov/labs/pmc/articles/PMC6232347/)
