# Persistent clotting protein pathology in Long COVID

- Lingering symptoms persist for as much as 6 months (or longer) after acute infection, where COVID-19 survivors complain of recurring fatigue or muscle weakness, being out of breath, sleep difficulties, and anxiety or depression.

- Circulating microclots are resistant to fibrinolysis(even after trypsinisation).

- Second trypsinisation dissolved the pellets formed from the first trypsinisation.

- Inflammatory molecules were trapped in the pellets that were dissolved.

> Of particular interest was a substantial increase in α(2)-antiplasmin (α2AP), various fibrinogen chains, as well as Serum Amyloid A (SAA) that were trapped in the solubilized fibrinolytic-resistant pellet deposits.

> Acute COVID-19 infection is also characterized by dysregulated, circulating inflammatory biomarkers, hyperactivated platelets, damaged erythrocytes and substantial deposition of microclots in the lungs

> Many patients are also developing Long COVID/PASC after mild or asymptomatic infection, despite not being hospitalized

## Terminologies

- Thrombocytes aka platelets. Thrombocytopenia refers to low blood platelet count. Platelets are crucial in forming blood clots. **Certain cancers, cancer treatments, medications and autoimmune diseases can cause the condition. Platelet levels often improve when you treat the underlying cause**. Thrombocytosis refers to high blood platelet count.

- The proteome is the entire set of proteins produced or modified by an organism or system. Proteomics refers to the study of proteomes and their functions.

## Normal and activated platelets

> Platelets, the smallest of our blood cells, can only be seen under a microscope. They’re literally shaped like small plates in their non-active form. A blood vessel will send out a signal when it becomes damaged. When platelets receive that signal, they’ll respond by traveling to the area and transforming into their “active” formation. To make contact with the broken blood vessel, platelets grow long tentacles and then resemble a spider or an octopus. -[Hopkins medicine](https://www.hopkinsmedicine.org/health/conditions-and-diseases/what-are-platelets-and-why-are-they-important)

Covid coagulopathy is beautifully explained by Doctor Jaco Laubscher in this [youtube video](https://youtu.be/W1eH6DYQ1ro)

## Class of people studied

- Healthy
- Type 2 Diabetes Mellitus
- Acute covid
- Long covid

## Summary/Highlights

> - Of particular interest was a substantial increase in the acute phase inflammatory molecule Serum Amyloid A (SAA4) and α(2)-antiplasmin (α2AP) that were trapped in the fibrinolytic-resistant pellet deposit.
> - The plasmin-antiplasmin system plays a key role in blood coagulation and fibrinolysis. Plasmin and α2AP are primarily responsible for a controlled and regulated dissolution of the fibrin polymers into soluble fragments
> - After this first trypsin digestion protocol, both COVID-19 and Long COVID/PASC samples formed a visible pellet deposit at the bottom of the tubes after centrifugation for 30 min at 13,000×g. Healthy PPP samples and the T2DM PPP samples did not form any visible deposit at the bottom of the tube.
> - Results suggest that Viscosity of PPP from patients with acute COVID-19 were the only samples showed an increase plasma viscosity.
> - Mass spectrometry based proteomics confirmed that levels of the coagulation factor XIII, α and β fibrinogen chains were increased in the digested pellet deposits from Long COVID/PASC samples compared to the digested plasma from controls and the digested pellet deposits from COVID-19. Relative to control and COVID-19 samples, plasminogen levels were slightly increased in the pellet deposits from Long COVID/PASC. SAA4 was not present in the digested plasma of the controls, but present in the digested pellet deposits of samples from Long COVID/PASC. It was a surprising result that SAA4 was so much increased in the digested pellet deposits. We suggest that these molecules are trapped and concentrated in the fibrinolysis-resistant microclots that are present in the circulation. SAA4 is a constitutively expressed molecule in contrast to SAA1 and 2 that are both acute phase proteins.
> - Central to COVID-19 pathology is the pathological roller-coaster from hypercoagulation and hypofibrinolysis. Bouck and co-workers in 2021 found that the lag times to thrombin, plasmin, and fibrin formation were prolonged with increased disease severity in COVID-19. The authors also argue that, although the presence of D-dimer suggests fibrinolytic pathways are intact and actively dissolving (lysing) fibrin, the discovery of fibrin deposits in lungs and other organs suggests dysregulation of the balance in fibrin-forming and fibrin-dissolving (plasmin generation) pathways is a major aspect of COVID-19 pathogenesis.
> - We also found increases in VWF in COVID-19 and Long COVID/PASC samples. This is of significance for platelet adhesion to endothelium. When VWF is higher, platelets will be more activated and more prone to adhesion to endothelium

![Fibrinolysis](images/fibrinolysis_pathology.jpg)

> - Here we conclude that
>
> 1. hypercoagulability due to significant increases in inflammatory molecules
> 2. circulating microclots and hyperactivated platelets, and
> 3. An aberrant fibrinolytic system, are all driven by a dysfunction in clotting protein and lytic enzyme supply and demand. Central to hypofibrinolysis and persistent microclots is the presence of a significant increase in α2AP.
> 4. The association of T2DM and the development of Long COVID/PASC needs urgent attention and further research is needed to determine the association between T2DM and Long COVID/PASC.

> **Clotting pathologies in both acute COVID-19 infection and in Long COVID/PASC might therefore benefit from a following a regime of continued anticlotting therapy to support the fibrinolytic system function**.

---

## References

- [Persistent clotting protein pathology in Long COVID/Post-Acute Sequelae of COVID-19 (PASC) is accompanied by increased levels of antiplasmin](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC8381139/)
