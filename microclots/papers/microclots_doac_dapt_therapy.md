# Combined triple treatment of fibrin amyloid microclots and platelet pathology in individuals with Long COVID

## Summary

- The most commonly reported Long COVID/PASC symptoms fatigue, brain fog, loss of concentration and forgetfulness, shortness of breath, as well as joint and muscle pains.

- A subset of 24 patients was treated with one month of **dual antiplatelet therapy (DAPT) (Clopidogrel 75mg/Aspirin 75mg) once a day, as well as a direct oral anticoagulant (DOAC) (Apixiban) 5 mg twice a day. A proton pump inhibitor (PPI) pantoprazole 40 mg/day was also prescribed for gastric protection**.

- Thromboelastography (TEG) was used to assist in determining their clotting status.

## Highlights

> In a large global study, a survey of 3,762 Long COVID/PASC patients from 56 countries found that nearly half still could not work full-time six months post-infection, due mainly to fatigue, post-exertional malaise, and cognitive dysfunction.

I too could not work fulltime.

### Genetic link

> Other pathophysiological mechanisms involving at least 20 metabolites may be involved, including a possible genetic link between the development of Long COVID/PASC and homocysteine(amino acid) metabolism

Homocysteine is an amino acid. **Vitamins B12, B6 and folate break down** homocysteine to create other chemicals your body needs. High homocysteine levels may mean you have a vitamin deficiency. Without treatment, elevated homocysteine increases your risks for dementia, heart disease and stroke.

> MTHFR enzyme function remains largely preserved when dietary folate intake is sufficient and therefore supplementation with B vitamins is suggested here.

**Homocysteine as a marker of inflammation that could be targeted to prevent endothelial damage and vascular comorbidities.** The value of a personalized approach based on randomized trials as previously conducted in hypertensive patients pre-screened for genetic variation in the MTHFR gene warrants further studies to elucidate the potential role of homocysteine accumulation in Long COVID/PASC.

Homozygosity for the MTHFR C677CT (rs1801133) variant or compound heterozygosity with A1298C (rs1801131) is the most common genetic cause of high homocysteine previously studied in COVID-19 patients.

### Treatment

- Anticoagulation is not needed on a long-term basis (unless there is anunderlying alternative indication).

- Once platelet activation and microclot formation returns to normal, indicating endothelial recovery, anticoagulation is stopped. The body is then able to return to managing normal clotting/physiology.

### Discussion

We show that hypertension and high cholesterol levels (dyslipidemia) are important comorbidities that may play a significant role in the development of Long COVID/PASC in this cohort.

The key point of importance in microclot formation in Long COVID/PASC is that fibrin(ogen) too can, in the presence of **various trigger substances**(spike protein a possible trigger?), fold into an amyloid form that has a very different macrostructure characterised by different fibre diameters and pore sizes.

In particular, it was found not only that the clots contained fibrin (fibrinogen after all being one of the most concentrated proteins in plasma) but that these microclots had entrapped many other proteins such as alpha-2-antiplasmin and a variety of other proteins and even antibodies, which therefore were not observed in plasma from which the microclots had been removed (so did not appear as biomarkers, even though they were there).

> This ‘triple therapy’ treatment therefore prevents platelet hyperactivation and also prevents new microclots from forming, **while it allows the body’s own fibrinolysis pathways to clear existing microclots**.

**NOTE**- If a bleeding tendency (not seen commonly) is a concern, a TEG blood test can be used to manage that, its role being as a safety-net to not overtreat the patient

## My takeaways

- TEG required before starting treatment to rule out hypocoagulation. Also PFA200 as mentioned by Doctor Jaco Laubscher.
- I would add CRP as well.
- Homocysteine test to check homocysteine metabolism
- Vitaming B profile, methylmalonic Acid, Folic acid tests?

  > We also provided convincing evidence that homocysteine levels are mediated by the effects of MTHFR C677T and the diet on BMI, which increases with a low folate score. There a low folate score correlates with a high BMI

- I don't know if we can even test for below genetic variation discussed. But still noting it down.
  > Genetic test to check for genes(particularly variation in MTHFR gene) that can affect homocysteine metabolism(DNA screening for genetic variation in the **MTHFR gene** to determine clinical relevance based on the personal and family medical conditions documented).

---

## References

- [Combined triple treatment of fibrin amyloid microclots and platelet pathology in individuals with Long COVID/ Post-Acute Sequelae of COVID-19 (PASC) can resolve their persistent symptoms](https://www.researchsquare.com/article/rs-1205453/v1)
