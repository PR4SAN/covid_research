# Impaired Fibrinolysis in severe covid

## Hypothesis

The procoagulatory state in COVID-19 patients is associated with **impaired fibrinolysis system**.

## Highlights

TODO

## My thoughts

- Can fibrinolysis be impaired in patients with mild Covid who go on to develop long covid?. We have patient experiences that documents clots in different organs months after acute covid.

- In long covid patients we definitely need to bring in blood tests that measure antiplasmin, tissue Plasminogen Activator, Plasminogen Activator Inhibitor 1 to see if the fibrinolysis is still impaired.

- Does this impaired fibrinolysis if present in long haulers is a permanent condition?

---

## References

- [Fibrinolysis Shutdown and Thrombosis in Severe COVID-19](https://www.ncbi.nlm.nih.gov/labs/pmc/articles/PMC7290169/)
- [High levels of plasminogen activator inhibitor-1, tissue plasminogen activator and fibrinogen in patients with severe COVID-19](https://www.medrxiv.org/content/10.1101/2020.12.29.20248869v1.full)
- [Severe SARS-CoV-2 infection inhibits fibrinolysis leading to changes in viscoelastic properties of blood clot: A descriptive study of fibrinolysis.](https://pubmed.ncbi.nlm.nih.gov/33634444/)
