# Eliquis as anticoagulant

## Eliquis treatment

I was on steroids while I was testing my self for microclots. Without steroids I was bedbound most of the time. So I thought I will use the energy given by steroids to get myself tested for microclots. I was working against my draining battery and fighting against time because I wont be on steroids for long.

Doctor was amazing and he told he will read up on microclots literature but at the same time since I insisted, he said doctors sometimes prescribe anticoagulants to patients coming off steroids on prophylaxis. So he prescribed me Eliquis (2.5mg) for 10 days. I hope in the meanwhile my doctor gets a chance to go through the microclot literature that I shared with him.

While I write this on December 28, I am not bedbound anymore and I can acutally work for 4 hours without exertion kicking in. And months of brain fog seem to go away post my first dose of AZ vaccine.

I am not celebrating it because I think it might be too soon to say, since its a path quite a few have already travelled(Apheresis folks).

But in this writeup I want to explore why I think eliquis seem to be helping for me.

## Why dig deep

I thought I will need both anticoagulants and antiplatelets to support my body with fibrinolysis and to calm the hyperactivated platelets that will prevent platelet aggregation too.

But doctor was confident in saying Eliquis has antiplatelet properties too. Apart from that he mentioned antiplatelet medicines would lead to increased bleeding risk given my age.

But after taking Eliquis I feel normal without joint, muscle aches that haunted me for almost 4 months. And I feel my inflammation has been brought down as well(yet to take CRP to see what the number is). So i was curious if Eliquis has anti-inflammatory properties too. I would love to measure my CRP now.

## What I didnt like about Eliquis?

God, the medicine is damn expensive.

## Eliquis as antiplatelet, anti-inflammatory and anticoagulant (Pleiotropic)

> This study aimed to explore whether there is any differences between direct thrombin and direct factor Xa inhibitors regarding the inhibiting effect against thrombogenesis in the clinical setting of acute ischemic stroke.

- Given I have microclots in my blood, I think its fair to assume that I am at the risk of getting a stroke, pulmonary embolism or heart attack though I am only 28 years old.

- I found a study where it discusses anticoagulant, antiplatelet and anti-inflammatory effects of Eliquis(among others).
- Patients only on antiplatelet are used as control(P), while patients on dabigatran and apixaban are named D and A group respectively.
- Apixaban has the capability to reduce inflammation as well as inhibit platelet aggregation to some extent(not to the extent of antiplatelet medicines)

## Eliquis

- Eliquis comes under NOAC(NonVitamin K antagonist Oral AntiCoagulant)
- NOACs have lower risk of hemorrhagic complications(bleeding)
- Additionally NOACs have shown to have anti-inflammatory as well as antiplatelet properties.

---

## References

- [Anti-inflammatory and antiplatelet effects of non-vitamin K antagonist oral anticoagulants in acute phase of ischemic stroke patients](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5768575/)
