# Covid as a vascular disease

- Microthrombi blocking microcapillaries in lungs where gas exchange happens - Hypoxia.

- Thrombotic microangipathy - micro clots in small blood vessels/capillaries.

- Clotting through enzymatic pathway and platelets activation pathway.

- Enzymatic pathway - Fibrinogen to fibrin.
- Activated Platelets - Platelets change shape, aggregation, forming a platelet conglomerate.

- Fibrinolysis - Plasminogen binds to clot activated by TPA from endothelium. As a result of fibrinolysis, the fibrin is broken down to d-dimer.

- Diseased endothelium leads to hypofibrinolysis.

- Measure the enzymatic pathway and platelet pathway using tests like PT, APTT, INR, TEG, Fibrinogen, PFA200(Platelet FunctionalAssay)

## Treatment

- Enzymatic pathway can be treated with DOAC(Direct Oral anticoagulant like eliquis)
- Platelet activation pathway can be treated with multiple antiplatelet drugs like aspirin, clopidogrel that target multiple platelet activation channels.

---

## References

- [Dr Jaco Laubscher Covid 19 Coagulopathy](https://www.youtube.com/watch?v=W1eH6DYQ1ro)
