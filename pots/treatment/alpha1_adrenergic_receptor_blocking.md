# COVID-19 outcomes among hospitalized men with or without exposure to alpha-1-adrenergic receptor blocking agents

---

## References

- [COVID-19 outcomes among hospitalized men with or without exposure to alpha-1-adrenergic receptor blocking agents](https://www.medrxiv.org/content/10.1101/2021.04.08.21255148v1.full)

- [Preventing cytokine storm syndrome in COVID-19 using α-1 adrenergic receptor antagonists](https://www.jci.org/articles/view/139642)
