# Prazosin to Prevent COVID-19

- Midodrine - alpha1 agonist. Used to treat orthostatic hypotension.
- Prazosin is alpha adrenergic antagonist

## My takeaway

- Currently this is in phase-2 trial
- How about using this test for POTS patients in long covid?

---

## References

- [Prazosin to Prevent COVID-19](https://clinicaltrials.gov/ct2/show/NCT04365257)
