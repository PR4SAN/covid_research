# Functional autoantibodies against G-protein coupled receptors in patients with persistent Long-COVID-19 symptoms

---

## References

- [Functional autoantibodies against G-protein coupled receptors in patients with persistent Long-COVID-19 symptoms](https://www.sciencedirect.com/science/article/pii/S2589909021000204)
