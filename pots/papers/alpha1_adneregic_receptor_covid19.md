# The Association Between Alpha-1 Adrenergic Receptor Antagonists and In-Hospital Mortality From COVID-19

---

## References

- [The Association Between Alpha-1 Adrenergic Receptor Antagonists and In-Hospital Mortality From COVID-19](https://www.frontiersin.org/articles/10.3389/fmed.2021.637647/full)
