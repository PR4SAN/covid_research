# GPCR autoantibodies in POTS

---

## References

- [Inflammatory Biomarkers in Postural Orthostatic Tachycardia Syndrome with Elevated G-Protein-Coupled Receptor Autoantibodies](https://www.ncbi.nlm.nih.gov/labs/pmc/articles/PMC7914580/)
- [Postural Orthostatic Tachycardia Syndrome Is Associated With Elevated G-Protein Coupled Receptor Autoantibodies](https://pubmed.ncbi.nlm.nih.gov/31495251/)
