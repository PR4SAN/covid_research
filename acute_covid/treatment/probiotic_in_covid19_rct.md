# Probiotic improves symptomatic and viral clearance in Covid19 outpatients: a randomized, quadruple-blinded, placebo-controlled trial

## Hypothesis

The probiotic may primarily act on the gut-lung axis (GLA) via crosstalk with the host’s immune system.

## Summary

- This study explores the gut-lung connection aka gut-lung axis.

- Subjects given probiotic formula containing **Lactiplantibacillus plantarum KABP022, KABP023, and KAPB033, plus strain Pediococcus acidilactici KABP021**.

- Probiotic use for 30 days 1 capsule a day before breakfast

- More complete remission of symptoms and viral load within 30 days in the priobiotic group compared to placebo.

- Patients in probiotic group had reduced nasopharyngeal viral load, lung infiltrates and reduced duration of symptoms like fever, cough, bodyache, shortness of breath, nausea, diarrhea and abdominal pain.

- Probiotic group had significant IgM and IgG antibody against the virus.

- No significant change in the fecal microbiome.

## Highlights

- All patients seropositive for Covid19 IgM

- Median time to overall symptom resolution (symptomatic clearance) was 5 days shorter in probiotic than placebo group

- L. plantarum and related Lactobacillaceae species such as Pediococci are common endophytes(organisms living on plants)

> The apparent lack of changes in fecal microbiome leads us to hypothesize that observed clinical effects are mediated either by bacterial molecules produced by the probiotic strains or the host microbiome’s adaptation to probiotic intake

## Limitations

> **Lymphopenia** is frequent in severe Covid19 patients and could potentially interfere with the antibody-stimulating activity of this probiotic, reducing its efficacy. Therefore, additional studies must be conducted before the use of this probiotic can be recommended to patients with severe Covid19.
> Effect on hospitalization, ICU stay, and mortality could not be assessed because of lack of occurrences during the study.

## My takeaways

- Probiotic medication was used during acute covid. But this study does not mention any information on whether the patients in the probiotic group went on to develop Long Covid.

- Possibly a larger RCT trial on this?

## References

- [Probiotic improves symptomatic and viral clearance in Covid19 outpatients: a randomized, quadruple-blinded, placebo-controlled trial](https://www.tandfonline.com/doi/full/10.1080/19490976.2021.2018899)
