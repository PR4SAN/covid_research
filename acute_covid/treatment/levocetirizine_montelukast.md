# Levocetirizine and montelukast in the COVID-19 treatment paradigm

## Summary

- exhibit remarkable anti-inflammatory activity
- may prevent disease progression
- prevent/treat Long Covid
- Large RCT clinical trial in approved by FDA

## Highlights

- Levocetirizine - antihistamine (H1 receptor antagonist). antiviral activity against human rhinovirus
- Montelukast - asthma, allergy and exercise induced bronchospasm. Potential main protease enzyme inhibition and virus entry to host cell via the ACE2

### NF-kB inhibition

> The downregulation of `NF-kB` is considered a key mechanism of action (MOA). NF-kB is expressed in almost all cell types and tissues.
>
> In a mouse model with SARS-cov1, Viral titers in the lung homogenates were similar in both untreated and treated animals, suggesting the reduction in proinflammatory cytokines after treatment with NF-kB inhibitors was not a consequence of reduced virus replication.
>
> One advantage of antivirals that target cellular protein activity in contrast to viral proteins lies in an effect not likely to be negated by mutations in the virus genome

## Levocetirizine MOA

- downregulates H1 receptor on the surface of mast cells and basophils

## Montelukast MOA

- inhibit the physiologic action of leukotriene.
- Leukotrienes are protein mediators of inflammation similar to histamines.

> Reported to inhibit the activation of NF-kB in a variety of cell types including monocytes/macrophages, T cells, epithelial cells, and endothelial cells, thereby interfering with the generation of multiple proinflammatory proteins

## Results

> Importantly, most patients treated with co-administration of levocetirizine and montelukast had symptom resolution within 7 days.
> Subjects with symptom resolution after 7 days typically had comorbidities that required a longer treatment period.
> In addition, no one in the study exhibited "Long COVID" symptoms greater than three months.

## Dosage

Levocetirizine 5mg and Montelukast 10mg orally once a day for 14 days for adults.

Treatment should be initiated within 5 days of symptoms onset for it to be effective.

## Study limitations

- This study is NOT a RCT

---

## References

- [Levocetirizine and montelukast in the COVID-19 treatment paradigm](https://pubmed.ncbi.nlm.nih.gov/34942461/)
