# The Structure of SARS-CoV-2

![Sars-cov2 structure](../images/sars_cov2_structure.jpg)

Single Stranded positive sense RNA which is larger than any other RNA viruses.

## Structure

Virus inaddition to the RNA viral genome, is comprised of the following structural proteins

- Spike glycoprotein (S)
- Envelope (E)
- Membrane (M)
- Nucleocapsid(N)

Nucleocapsid forms the outside of genome. Genome is packed by an envelope consisting of Spike, Membrane and Envelope proteins.

It also consists of 16 non-structural proteins(nsp 1-16).

## Spike glycoprotein

- Critical for entry to the host cell
- Consists of two funcional subunits namely S1 and S2 subunits
- S1 consists of N-terminal domain (NTD) and receptor binding domain (RBD). Function of S1 is to bind to the receptor on the host cell.
- S2 contains fusion peptide(FP), heptad repeat 1 (HR1), central helix (CH), connector domain (CD), heptad repeat 2 (HR2), transmembrane domain (TM), and cytoplasmic tail (CT). Its function is to fuse the membranes of the viruses and host cells.
- S1/S2 cleavage site is called as S1/S2 protease cleavage site.
- In the case of SARS-CoV and SARS-CoV-2, to enter host cells, they recognize the receptor angiotensin-converting enzyme 2 (ACE2) on host cells via the receptor binding domain (RBD).
- S protein has two states closed and open. In open state, RBD is in the UP conformation and is necessary for the fusion of the virus to the host cell membranes.

## Host cell entry

### ACE2

> The affinity between ACE2 and SARS-CoV-2 is higher than the affinity between ACE2 and SARS-CoV.

ACE2 receptor is kind of ubiquitous in human body.

> ACE2 was shown to be abundantly present in human epithelial cells of the lung and enterocytes of the small intestine as well as in endothelial cells of the arterial and venous vessels. - [Epithelial and Endothelial Expressions of ACE2: SARS-CoV-2 Entry Routes][2]

### CD147

- [CD147 receptor][1] a transmembrane protein receptor has been identified as another binding target for the S protein. CD147 is highly expressed in inflammed, pathogen infected cells and tumor cells.

- CD147 could facilitate viral entry to the host cell via endocytosis(catch and clump). Endocytosis is a cellular process in which substances are brought into the cell.

**NOTE**- There are papers that contradict this receptor involvement.

---

## References

- [The Structure of SARS-CoV-2](https://www.frontiersin.org/articles/10.3389/fcimb.2020.587269/full)

- [CD147-spike protein interaction in COVID-19][1]

- [Epithelial and Endothelial Expressions of ACE2: SARS-CoV-2 Entry Routes][2]

[1]: https://www.sciencedirect.com/science/article/pii/S0048969721071485
[2]: https://pubmed.ncbi.nlm.nih.gov/33626315/
