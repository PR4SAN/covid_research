# Immunoglobulin signature predicts risk of post-acute COVID-19 syndrome

## Terminologies

- PACS - Post Acute Covid Syndrome

## Finding

> We discover an immunoglobulin (Ig) signature, based on
>
> - Total IgM and IgG3 levels, which – combined with
> - Age,
> - History of asthma bronchiale, and
> - Five symptoms during primary infection

is able to predict the risk of Post Acute Covid Syndrome (PACS) independently of timepoint of blood sampling.

## Highlights

Factors correlating with PACS

- hospitalization
- comorbities such as lung disease, asthma, heart disease
- age
- sex
- first week symptoms

175 RT-PCR covid positive and 40 healthy controls. The experimental group consisted of asympomatic, mild, mild pneumonia, severe pneumonia, severe covid (ARDS) patients.

The most common prolonged symptoms were fatigue, dyspnea, a change in smell or taste, and anxiety or depression.

### Acute covid Factors for PACS

- Hospitalized and Asthma patients tend to develop PACS.
- CRP and TNF concentrations (inflammatory markers) were higher during the acute covid stage.
- Higher titers of S1 specific IgA and IgG found in severe cases.
- Decreased IgM in PACS patients at both acute and 6-month follow-up, IgG1 was unaltered and IgG3 was less in patients with PACS
- Individuals with either low IgM or low IgG3 had an increased risk of developing PACS, whereas patients with both high IgM and high IgG3 were less likely to develop PACS
- The symptom count during primary infection correlated with the maximal followed-up disease severity of COVID-19 patients.

---

## References

- [Immunoglobulin signature predicts risk of post-acute COVID-19 syndrome](https://www.nature.com/articles/s41467-021-27797-1)
