# Afucosylation of anti-SARS-CoV-2 IgG

## Terminologies

- Immune complex - Antibody and its binding antigen forms an immune complex.

This image explains the breaking of pulmonary blood barrier.

![Cytokines and inflammation](../images/cytokines_and_inflammation.jpeg)

Similar endothelial barrier can be broken in brain too, refer below image.

![Neuroinflammation](../images/neuroinflammation.svg)

## 1. Early non-neutralizing, afucosylated antibody responses are associated with COVID-19 severity

This [study][1] was covered in [this medicalxpress article.](https://medicalxpress.com/news/2022-01-antibodies-blood-covid-onset-severity.html). [This study][2] is also from the same authors of [above study][1]

In essence the study says, those who go on to develop severe Covid19 are those with

- in the initial days of the infection, have high titers of antibodies(IgG) to SARS-Cov2 which lack fucose sugar chains (fucose deficient antibodies)
- Abundance of **CD16a** receptors on the monocytes(immune cell)

Above combination triggers release of **inflammatory cytokines(IL-6 and TNF)**(cytokine storm) as seen in the Severe covid patients.

Authors of [**this study**][1] propose **using the fucose deficient antispike antibodies as a biomarker for the disease progression**.

> These results show that **`IgG-FcγR` interactions** are able to regulate inflammation in the lung and may define distinct lung activities associated with the IgG that are associated with severe COVID-19 and protection against infection with SARS-CoV-2

These severely affected folks go on to develop neutralizing antibodies only in the later stages of the infection. People with mild infection were capable of developing fucose rich neutralizing antibodies earlier on in the infection. Vaccinated people were also developing fucose rich neutralizing antibodies.

> Together, these findings support a mechanism in which afucosylated ICs in the lung trigger CD16a-dependent production of chemokines which promote subsequent influx of innate immune cells.

## 2. High titers and low fucosylation of early human anti-SARS-CoV-2 IgG promote inflammation by alveolar macrophages

This is another study that discusses the **high titers of low fucosylated sars-cov2 spike antibody complex expressed by infected cells** early during the infection activate the macrophages which in turn release proinflammatory cytokines **disrupting the pulmonary endothelial barrier** subsequently causing microvascular thrombosis through platelet activation.

This paper proposes using **fostamatinib** to counteract the macrophage activation by fucose deficient antispike-IgG.

### Highlights

- A detailed assessment of the cytokine profile in severe cases of COVID-19 indicates that some cytokines and chemokines are strongly elevated, such as interleukin (IL)-6, IL-8, and tumor necrosis factor (TNF).

- In contrast, type I and III interferon (IFN) responses, which are critical for early antiviral immunity, appear to be suppressed.

- In both rheumatoid arthritis and SARS-CoV-1 infection, IgG antibodies convert wound-healing **“M2” macrophages to a proinflammatory phenotype**.

- Spike-IgG antibody immune complexes elicited small amounts of IL-1β, IL-6, and TNF but very high IL-8 production by human macrophages

- These cytokines were not seen in severely ill covid patients who tested negative for these spike antibodies.

- Fucosylation was comparable in mild as well as severe covid 19 patients, only difference was the titers of antispike was low in mild patients.

- **Can this low titer of antispike IgG in mild/moderate patients lead to Long Covid**? Even some level of inflammatory cytokine response could damage/disrupt endothelium (endothelialitis?). Also we know from InCelldx panel many have elevated inflammatory cytokines.

- High anti-spike titers drive proinflammatory cytokine production by human macrophages.

- **IgG-opsonized spike-expressing 293F cells, which mimic SARS-CoV-2–infected cells and induce the formation of larger immune complexes**, did amplify IL-6 production by macrophages.

- The Afucosylated anti–SARS-CoV-2 IgG promotes **inflammation by engagement of IFN pathways**, which are classical cofactors to promote macrophage activation

- In patients critically ill with COVID-19, the first anti-spike IgG antibodies that are produced after infection are intrinsically more inflammatory by bearing different glycosylation patterns.

- We observed significantly increased platelet adhesion to endothelium. This effect was paralleled by an increase in von Willebrand factor release from the endothelial cells, indicative of an active procoagulant state of the endothelium.

## 3. Complement activation induces excessive T cell cytotoxicity in severe COVID-19

Profound T cell activation and induction of **CD16 expressing** helper, killer and TReg T cells in severe COVID-19

### Key points

- CD16+ expressed cytotoxic T cells are activated by the antispike-antibody immune complex, their infiltration could cause alveolar lung damage and endothelial cell injury which in turn releases chemokines. These chemokines play a vital role in increased infiltration of neutrophils and macrophages.

- Immune complex-mediated degranulation of CD16+ T cells contributes to endothelial injury.

- CD16A expressing T cell clones induced during acute severe COVID-19 persist and maintain their increased cytotoxic potential.

- High proportions of activated CD16+ T cells and plasma complement protein levels are associated with a worse outcome of COVID-19

## My takeaways

> The transmembrane protein CD16 (Fc gamma RIII) is detected on activated macrophages, natural killer (NK) cells and a small subset of T lymphocytes. - [CD16](https://pubmed.ncbi.nlm.nih.gov/1376268/)

All of them are turned to cytotoxic phenotypes through afucosylated antispike-antibody immune complex as these bind to CD16a receptor on these cells and CD16a/afucosylation increases Antibody Dependent Cell-mediated Cytotoxicity(ADCC). I wonder if these activations can be confirmed by looking at the cytokine profile.

- Thought - Rapid antibody tests might incorporate detection of fucose deficient antispike-antibodies in the future?

- Fostamatinib needs RCT, its very promising as per the paper. This could stop progression of disease when administered at early stage in the illness.

- This spike-antibody triggered macrophage activation resulting in endothelium damage causes platelet activation and subsequent clotting cascade(microclots). Endothelium functioning should be studied in long covid patients.

- This could be the reason why triple treatment combo immediately post covid in long haulers seem to get rid of brain fog, fatigue, muscle weakness and joint aches(possible fibrin deposition(microclot) due to imparied fibrinolysis from damaged endothelium). So even in patients with mild infection, based on TEG, CT score etc, low dose anticoagulants should be prescribed during the followup.

- This process could happen in some vaccinated people. It will be interesting to know if those who developed high antibody titers go on to develop LC like symptoms.

- And if we have CD16 receptor related cytokines observed in covid and vax LC patients then atleast we have some clue. **I think Incelldx tests focusses on these cytokines, chemokines. Those doctors might have the much needed data**.

---

## References

- [Early non-neutralizing, afucosylated antibody responses are associated with COVID-19 severity][1]
- [Proinflammatory IgG Fc structures in patients with severe COVID-19][2]
- [High titers and low fucosylation of early human anti-SARS-CoV-2 IgG promote inflammation by alveolar macrophages][3]
- [Complement activation induces excessive T cell cytotoxicity in severe COVID-19][4]

[1]: https://www.science.org/doi/10.1126/scitranslmed.abm7853
[2]: https://www.medrxiv.org/content/10.1101/2020.05.15.20103341v2
[3]: https://www.science.org/doi/10.1126/scitranslmed.abf8654
[4]: https://pubmed.ncbi.nlm.nih.gov/35032429/
