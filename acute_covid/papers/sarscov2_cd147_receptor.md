# CD147-spike protein is a novel route for SARS-CoV-2 infection to host cells

- [CD147 receptor][1] a transmembrane glycoprotein receptor has been identified as another binding target for the S protein.
- CD147 is highly expressed in inflammed, pathogen infected cells and tumor cells.
- CD147 is expressed on all leukocytes, platelets, and endothelial cells"

## Highlights

Below observations are **not invivo** rather invitro

> - CD147 interacts with the spike protein
> - SARS-CoV-2 employs the CD147 receptor for host cell entry
> - CD147 mediates SARS-CoV-2 infection in hCD147 mice
> - CD147 is an alternative receptor for SARS-CoV-2 infection in ACE2-deficient cell types
>   SARS-CoV-2 enters the host cells through CD147-mediated endocytosis

### CD147 is an alternative receptor for SARS-CoV-2 infection in ACE2-deficient cell types

> Previous study provides the evidences that lymphocytes are infected and injured by SARS-CoV, leading to decreased circulating lymphocytes.
>
> Interestingly, in our study, SARS-CoV-2 virions were observed in lymphocytes in lung tissue from COVID-19 patient.
>
> T cells derived from human peripheral blood mononuclear cells (PBMC) were collected to analyze the expressions of CD147 and ACE2 by real-time PCR assay, which demonstrated that CD147 expression was detected in CD4+ and CD8+ T cells, but no expression of ACE2 was detected.
>
> CD147 is also reported to be significantly upregulated in activated T lymphocytes.
>
> Here, we confirmed that the expression of CD147 was significantly increased in both activated CD4+ and CD8+ T cells, however, the expression of ACE2 was not increased
>
> Virus infection assays showed that T cells were infected with SARS-CoV-2 **pseudovirus** in a dose-dependent manner, and significantly enhanced infection of pseudovirus was observed in activated T cells, compared to unactivated T cells
>
> These results indicate that CD147 is an alternative receptor for SARS-CoV-2 infection, especially in ACE2-deficient cell types.

## My takeaways

- There are [some recently published papers][2] that oppose the CD147-spike interaction.

## References

- [CD147-spike protein is a novel route for SARS-CoV-2 infection to host cells][1]

[1]: https://www.nature.com/articles/s41392-020-00426-x
[2]: https://pubmed.ncbi.nlm.nih.gov/33432067/
