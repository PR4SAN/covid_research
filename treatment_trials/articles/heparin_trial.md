# Clinical trial shows common drug heparin can be used to improve COVID-19 symptoms

- [Study][1] co-authored by scientists from the Australian National University and King's College in London. **This is not an RCT**.

## Highlights

- Heparin given via inhaling using a nebuliser to avoid the bleeding risk.
- Breathing and oxygen improved in 70%
- Heparin stops virus from entering the cells due to its antiviral and antiinflammatory properties.
- Heparin also acts as an anticoagulant

> "Inhaled heparin has antiviral properties which work by binding to the spike proteins the coronavirus uses to enter the cells of the body," Professor Page said.

**RCTs required** to confirm the efficacy of heparin.

## Heparin Pros

- Well known existing drug
- Cheap
- Inhaled heparin is safe and effective

## Heparin Cons

- Associated with **bleeding** risk since it acts as anticoagulant.
- Heparin when administered **intraveously**(not when nebulised), in some cases can cause thrombocytopenia which is considered as adverse reaction to the drug.

---

## References

- [Clinical trial shows common drug heparin can be used to improve COVID-19 symptoms](https://amp.abc.net.au/article/100767852)

[1]: https://pubmed.ncbi.nlm.nih.gov/34984714/
