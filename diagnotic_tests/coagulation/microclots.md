# Microclots

To know about the microclot literature that made me do these, please refer to [persistent clotting in PASC](../papers/persistent_clotting_pasc_resiap/notes.md)

## Microclots detection process

![Microclot detection process](images/microclots_procedure.png)

Process is described in detail [in this paper from Professor Resia Pretorius and team](https://www.mdpi.com/2077-0383/10/22/5381)

## Microclots detection

After finding out microclots in my blood, I discussed with my doctor on treatment options. While research from Resia Pretorius, treatment from Doctors like Beate Jaeger(Germany), Jaco Laubscher shows treating with multiple blood thinners that includes anticoagulants(Heparin/Eliquis etc), antiplatelets(Aspiring, Clopidogrel) seem to be helping patients, but my doctor raised the concern of potential bleeding from those set of medications and asked if I have any research paper on the same. Luckily in the subsequent week, paper was published [Combined triple treatment of fibrin amyloid microclots and platelet pathology in individuals with Long COVID/ Post-Acute Sequelae of COVID-19 (PASC) can resolve their persistent symptoms](https://www.researchsquare.com/article/rs-1205453/v1)

[This latest paper on treatment using DOAC and DAPT from team clots](../papers/pasc_triple_treatment_resiap/notes.md) has described on how to quantitatively categorize(my understanding) people with microclots(severity). So I shared this with my doctor to see if we can work on something.

![Microclots in Girish Blood](images/girish_microclots.jpeg)
