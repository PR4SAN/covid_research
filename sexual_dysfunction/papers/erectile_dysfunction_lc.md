# The Sexual Long COVID (SLC): Erectile Dysfunction as a Biomarker of Systemic Complications for COVID-19 Long Haulers

- ACE2 receptor is ubiquitous. Virus enters the cell through this receptor.

- Endothelial cells invaded through ACE2. Endothelial dysfunction is reported in long covid.

- Presence of **chronic low-grade inflammation** can trigger symptoms of long COVID.

## Neuropsychiatric Complications of Long COVID

- Endothelial dysfunction in the brain vessels.

- Encephalitis(brain inflammation) can also be caused by covid.

- Blood-brain barrier - A network of blood vessels and tissue that is made up of closely spaced cells and helps keep harmful substances from reaching the brain. The blood-brain barrier lets some substances, such as water, oxygen, carbon dioxide, and general anesthetics, pass into the brain. It also keeps out bacteria and other substances.

- CSF - Cerebrospinal fluid is a clear, colorless body fluid found within the tissue that surrounds the brain and spinal cord of all vertebrates. Covid is identified in CSF also.

- Fatigue, headache and brain fog are most commonly reported symptoms.

- Brain fog **is a colloquialism used to define** the impairment in cognitive functions featuring confusion, sluggishness of thought and difficulties concentrating.

- Olfactory(smell) and gustatory(taste) dysfunctions reported in long covid.

- Anxiety and depression

## Respiratory Complications of Long COVID

- Shortness of breath, Pulmonary dysfunction

> The inflammatory response (particularly interleukin-6 which is among the main drivers of inflammation for COVID-1996) and the **endothelial dysfunction occurring in the small vessels of the pulmonary circulation** induce pulmonary dysfunction

## Cardiovascular Complications of Long COVID

- Chest pain, tachycardia, myocarditis, coagulopathy, POTS

- Severity of endothelial dysfunction is proportional to occurence of thrombotic events.

## Endocrine Complications of Long COVID

> ACE2 is highly expressed by several cell types,141 including many highly relevant for the endocrine system such as pancreatic β-cells, hypothalamic and pituitary cells and testicular Leydig and Sertoli cells.

> COVID-19 can also lead to the onset of type I diabetes mellitus in predisposed individuals following autoimmune reaction towards the β-cells,160 as well as to accelerated development of type II diabetes mellitus due to pancreatic inflammation.

- Hypogonadism possible from testicular damage, can affect reproductive health, male fertility.

## Relevance of above complications for Erectile Function

- Erectile function is strictly dependent on the integrity of the endothelium

> As **sexual activity can be considered a mild form of physical exercise**, accounting for 3-5 metabolic equivalents, it can become somewhat hazardous in patients with myocarditis or with a sudden onset of chest pain - even more in case of pulmonary dysfunction

- Reduced testosterone is a problem.

## Conclusion

Factors possibly contributing to Sexual dysfunctions (particularly concerning ED)

- Endothelial dysfunction
- Prolonged hypoxia due to respiratory impairment
- Anxiety and depression
- Endocrine disorders

---

## References

- [The Sexual Long COVID (SLC): Erectile Dysfunction as a Biomarker of Systemic Complications for COVID-19 Long Haulers](https://www.sciencedirect.com/science/article/pii/S2050052121000858)
