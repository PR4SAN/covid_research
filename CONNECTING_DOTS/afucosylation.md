# Afucosylated SARS-cov2 antispike antibodies

I read few papers from different authors regarding the role of afucosylated antispike-antibody immune complex during early days of infection as a possible indicator of disease severity.

Afucosylation refers to absence of fucose sugar chain on the antibody tail to which the immune cells bind to. Antibody Fucosylation has shown to be helpful in predicting disease severity in dengue.

Basically cells that have these afucosylated antispike-antibody complex on its surface will be recognized by immune system to get killed aka Antibody Dependent Cellular Cytotoxity(ADCC). These fucose deficient antibodies bind to receptors "CD16A" which is known to be expressed by macrophages, natural killer cells, subset of T cells.

This receptor is known to trigger proinflammatory response. All the below papers refer to this receptor playing a major role in triggering proinflammatory response.

Key points common to these papers are

1. High titers of fucose deficient antispike antibody(IgG) early in the infection
2. Endothelial injury from activated macrophages, cytotoxic T cells
3. Platelet activation and microthrombosis

Later body seems to produce fucosylated Abs.

Any of us can develop afucosylated antispike antibodies to spike either from live virus or from the vaccine. If that happens, then endothelial injury can happen anywhere in the body be it brain, lungs is my assumption.

If its a live virus, people seem to progress to severe disease(as per the paper). In mild cases, very low - low titers of these afucosylated antispike-antibodies are observed.

I am **thinking** it is possible in both LC from mild/moderate covid, vax injured(not from wrongly injecting into bloodstream) seem to develop moderate titers of these afucosylated Abs early phase that seem to cause endothelial injury, platelet activation and microclot formation.

This could explain the resemblence in the similarity (to some extent)of long covid symptoms between LC folks and vax injured folks. Those who are vax injured(previously healthy) in that aspect **could be lucky** to escape adversity from live virus IMO.

**Hopefully** for all the hit taken by vax injured, all the vaccinated could go on to develop memory B-cells capable of generating fucosylated Abs in the first place protecting from severe disease when infected in future.Because in the end the study shows all Abs were fucosylated

Endothelial injury and disruption means impaired fibrinolysis. So clots seem to persist and cause a wide range of symptoms in LC. And in LC and MECFS folks who are known to have ongoing endothelial damage + afucosylated Abs(incase of ME) could react badly to vax.

The cytokine panel from Incelldx could tell us if we have some common cytokines, interferons and chemokines in both LC from virus and LC from vax. That could give us some idea.

By the way once the endothelial barrier is broken, every organ can be damaged in its own ways.

Afucosylation is done to promote ADCC, so it's really not bad. It just indicates those people with high titers of such antibodies early in the infection, have so much cells invaded by the virus. So in these people virus is somehow replicating a lot faster.

As the authors of one of these papers say this could be used as a biomarker for disease progression.I wish this research makes deeper strides. Imagine if we can test for "this" as part of our Rapid Antigen Test apart from just positivity,it can save lives with early intervention.

## References

- [Early non-neutralizing, afucosylated antibody responses are associated with COVID-19 severity][1]
- [Proinflammatory IgG Fc structures in patients with severe COVID-19][2]
- [High titers and low fucosylation of early human anti-SARS-CoV-2 IgG promote inflammation by alveolar macrophages][3]
- [Complement activation induces excessive T cell cytotoxicity in severe COVID-19][4]

[1]: https://www.science.org/doi/10.1126/scitranslmed.abm7853
[2]: https://www.medrxiv.org/content/10.1101/2020.05.15.20103341v2
[3]: https://www.science.org/doi/10.1126/scitranslmed.abf8654
[4]: https://pubmed.ncbi.nlm.nih.gov/35032429/
