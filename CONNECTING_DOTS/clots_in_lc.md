# Clots in LC patients

Clotting happens in Covid 19. Macrophages, T cells release inflammatory cytokines which inturn could activate platelets and trigger the clotting pathology.

## Elevated D-Dimer

Endothelium functioning but lots of endothelial cells damaged leading to coagulation -> Fibrinolysis -> elevated D-dimer -> Treat with anticoagulants

## Normal D-Dimer

Endothelial injury + dysfunction? -> Impaired Fibrinolysis -> Normal D-Dimer -> Fibrin clots remain -> Get in to microcapillaries -> Impair oxygen exchange -> Cause Necrosis and clotting events.

## Endothelial impairment

- Virus invades endothelial cells and immune system kills those antigen presenting endothelial cells(process known as Antibody Dependent Cellular Cytotoxicity)

How can endothelial dysfunction be addressed therapeutically?

## Blood work

See if any of these Protein C, S and antithrombin (natural anticoagulants) are reduced. Also if we also have elevated antiplasmin, plasminogen activator inhibitor 1, reduced tissue plasminogen activator etc.
