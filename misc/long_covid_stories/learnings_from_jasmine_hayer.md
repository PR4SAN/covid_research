# Long Covid story of Jasmine Hayer

## Highlights

> - This illness is so baffling and no-one really knows how to treat it. I honestly don't know if I will ever get back to full health but I'll never stop trying. I can't even lift my left arm up.
> - For nine months, doctors said anxiety was the cause of her symptoms, which included a tight chest, heart pain, breathlessness, fatigue and palpitations.
> - They also found small blood clots on her lungs, which only showed up on a specialised scan called a VQ scan. Since starting blood-thinning medication, the clots have gone but she still has abnormal blood and oxygen flow to her lungs.
> - Doctors don't know why I have good overall levels of oxygen in my body but it's not getting to my lungs, which could be an issue with my blood vessels, but my scans show they are normal - they've never seen this before.

Jasmine also mentions about HELP apheresis and microclot study.

## My takeaways

Its a nice idea to start using your own [symptom tracker](https://www.mysymptomtracker.co.uk/). All these days I relied upon my memory only to find out how unreliable my memory is.

### Tests for diagnosis

They involved a variety of

- Lung function tests which included: a capillary blood gas test, peak flow test, spirometry and a gas transfer test (also known as lung diffusion).
- a CT scan,
- Dual-energy CT pulmonary angiogram to look for clots in the lungs,
- a 24/7 ECG,
- Blood tests like thyroid function, B12, folate, d-dimer, troponin, ferritin, BNP(Brain Natriuretic Peptide) marker
- The basic’s (ECG and baseline bloods).
- Echocardiogram to access heart damage. Echo can show pericardial diffusion(fluid build up around heart). Post that cardiac MRI might be required.
- Based on the outcome of lung gas transfer test(less than 80%), patient might be ordered VQ scan(ventilation perfusion scan). VQ scan involves two scans: one measures how well air is able to flow through your lungs and the other measures where blood flows in your lungs.
- [BNP marker blood test](https://www.gloshospitals.nhs.uk/our-services/services-we-offer/pathology/tests-and-investigations/b-type-natriuretic-peptide-nt-probnp/) to detect presence of any heart problem
- A cardiopulmonary exercise test (CPET) lets your doctor see how your lungs, heart and muscles react together when you exercise.

### Medications

- Apixaban(Eliquis) as blood thinner.
- Colchicine as antiinflammatory.

---

## References

- [Long Covid: 'I have to choose between walking and talking'](https://www.bbc.com/news/uk-england-beds-bucks-herts-59584146)
- [Jassmine Hayer's blog](https://mylongcoviddiaries.medium.com/)
