# Blood clotting in LongCovid, MECFS, POTS & Lyme

- Fibrin amyloid microclots
- Fibrin autoantibodies
- BC007 targets autoantibodies that affect micro-circulation.
- Hypercoagulation - Possible genetic predisposition for clotting.
- Increased blood viscosity - supply of O2 and nutrients
- APS - AntiPhospholipid Syndrome. Antiphospholipid antibodies (well recognized systemic autoimmune disease) in other [chronic illnesses](https://www.healthrising.org/blog/2018/08/15/sticky-blood-antiphospholipid-syndrome-pots-chronic-fatigue-syndrome-and-fibromyalgia/) like [**Lyme disease**](https://pubmed.ncbi.nlm.nih.gov/21729977/)

> APS occurs when increased levels of antibodies attack the phospholipids or fats in our cells and in the lining of our blood vessels. (You can have the antibodies without having the disease.)

---

## References

- [Blood clotting in LongCovid, MECFS, POTS & Lyme](https://twitter.com/Be_Kinderr/status/1476999743238807556)
